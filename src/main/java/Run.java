import domain.*;

import java.time.LocalDateTime;

public class Run {
    public static void main(String[] args) {
        Order order = new Order(999, true);

        Movie JamesBond = new Movie("James Bond");
        Movie ChickenLittle = new Movie("Chicken Little");

        MovieScreening movieScreening1 = new MovieScreening(JamesBond, LocalDateTime.now(), 5);
        MovieScreening movieScreening2 = new MovieScreening(ChickenLittle, LocalDateTime.now(), 10);
        MovieScreening movieScreening3 = new MovieScreening(ChickenLittle, LocalDateTime.now(), 10);
        MovieScreening movieScreening4 = new MovieScreening(JamesBond, LocalDateTime.now(), 5);

        MovieTicket ticket1 = new MovieTicket(movieScreening1, false, 3, 3);
        MovieTicket ticket2 = new MovieTicket(movieScreening2, false, 8, 7);
        MovieTicket ticket3 = new MovieTicket(movieScreening3, false, 4, 8);
        MovieTicket ticket4 = new MovieTicket(movieScreening4, false, 6, 1);

        order.addSeatReservation(ticket1);
        order.addSeatReservation(ticket2);
        order.addSeatReservation(ticket3);
        order.addSeatReservation(ticket4);

        System.out.println("Total price: " + order.calculatePrice());

        order.export(TicketExportFormat.PLAINTEXT);
    }
}
