package domain;

import java.util.ArrayList;
import java.io.FileWriter;   // Import the FileWriter class
import java.io.IOException;  // Import the IOException class to handle errors
//import org.json.*;

public class Order
{
    private int orderNr;
    private boolean isStudentOrder;
    private String day;

    private ArrayList<MovieTicket> tickets;

    public Order(int orderNr, boolean isStudentOrder)
    {
        this.orderNr = orderNr;
        this.isStudentOrder = isStudentOrder;

        tickets = new ArrayList<MovieTicket>();
    }

    public int getOrderNr()
    {
        return orderNr;
    }

    public void addSeatReservation(MovieTicket ticket)
    {
        tickets.add(ticket);
    }

    public double calculatePrice()
    {
        double price = 0;
        double sum = 0;
        int i = 1;

        String day = "mon";

        for (MovieTicket ticket:tickets) {
            price = ticket.getPrice();

            if(!(IsFree() && i % 2 == 0)) {
                if (ticket.isPremiumTicket()) {
                    if(isStudentOrder)
                        price = price + 2;
                    else
                        price = price + 3;
                }

                sum += price;
            }

            i++;
        }

        //Discount
        if(day == "sat" || day == "sun")
        {
            if(!isStudentOrder && tickets.size() >= 6)
            {
                sum = (sum / 100) * 90;
            }
        }

        return sum;
    }

    public boolean IsFree()
    {
        if(isStudentOrder)
        {
            return true;
        }
        else
        {
            if(day == "mon" || day == "tue" || day == "wed" || day == "thu")
            {
                return true;
            }
        }

        return false;
    }

    public void export(TicketExportFormat exportFormat)
    {
        // Bases on the string respresentations of the tickets (toString), write
        // the ticket to a file with naming convention Order_<orderNr>.txt of
        // Order_<orderNr>.json

        if(exportFormat.equals(TicketExportFormat.PLAINTEXT)) {
            try {
                FileWriter myWriter = new FileWriter("Order_" + orderNr + ".txt");
                myWriter.write("Tickets of Order #" + orderNr + ": \n\n");

                for (MovieTicket ticket:tickets) {
                    myWriter.write(ticket.toString() + "\n");
                }

                myWriter.close();
                System.out.println("Successfully wrote to the file.");
            } catch (IOException e) {
                System.out.println("An error occurred.");
                e.printStackTrace();
            }
        }
        else if(exportFormat.equals(TicketExportFormat.JSON))
        {
            //Creating a JSONObject object
//            JSONObject jsonObject = new JSONObject();
//            //Inserting key-value pairs into the json object
//            jsonObject.put("ID", "1");
//            jsonObject.put("First_Name", "Shikhar");
//            jsonObject.put("Last_Name", "Dhawan");
//            jsonObject.put("Date_Of_Birth", "1981-12-05");
//            jsonObject.put("Place_Of_Birth", "Delhi");
//            jsonObject.put("Country", "India");
//            try {
//                FileWriter file = new FileWriter("E:/output.json");
//                file.write(jsonObject.toJSONString());
//                file.close();
//            } catch (IOException e) {
//                // TODO Auto-generated catch block
//                e.printStackTrace();
//            }
//            System.out.println("JSON file created: "+jsonObject);
        }
    }
}
